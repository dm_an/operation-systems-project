#ifndef SEMAPHORES_H_
#define SEMAPHORES_H_

#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#define paintAccess 0
#define partExists  1
#define paintedPart 2
#define ready1      3
#define written1    4
#define ready2      5
#define written2    6
#define ready3      7
#define written3    8

#define KEY_SEM 3333

/* Union for semaphores */
union semun
{
	int val;
	struct semid_ds *buff;
	unsigned short *array;
};

int createSem(int numOfSemaphores, int initValue);
void up(int semid, int numOfSem);
void down(int semid, int numOfSem);
int accessSem(int numOfSemaphores);
void destroySem(int semid);

#endif
