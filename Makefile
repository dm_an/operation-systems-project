all:	Construct  Check  Assembler  Paint  Starter

Construct:	Construct.o Semaphores.o SharedMemory.o
	g++ -o Construct Construct.o Semaphores.o SharedMemory.o

Check: Check.o Semaphores.o SharedMemory.o
	g++ -o Check Check.o Semaphores.o SharedMemory.o

Assembler: Assembler.o Semaphores.o SharedMemory.o
	g++ -o Assembler Assembler.o Semaphores.o SharedMemory.o

Paint: Paint.o Semaphores.o SharedMemory.o
	g++ -o Paint Paint.o Semaphores.o SharedMemory.o

Starter: Starter.o Semaphores.o SharedMemory.o
	g++ -o Starter Starter.o Semaphores.o SharedMemory.o
	
Construct.o: Construct.cpp
	g++ -c Construct.cpp

Check.o: Check.cpp
	g++ -c Check.cpp

Assembler.o: Assembler.cpp
	g++ -c Assembler.cpp

Paint.o: Paint.cpp
	g++ -c Paint.cpp
	
Starter.o: Starter.cpp
	g++ -c Starter.cpp

Semaphores.o: Semaphores.cpp
	g++ -c Semaphores.cpp
	
SharedMemory.o: SharedMemory.cpp
	g++ -c SharedMemory.cpp

clean:
	rm Construct Check Assembler Paint Starter *.o
	
