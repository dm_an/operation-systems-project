#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <signal.h>
#include "Semaphores.h"
#include "SharedMemory.h"
#include "Part.h"

#define S 10000

using namespace std;

double getAssembleTime(Part &p1, Part &p2, Part &p3, char* prodId)
{
	double ms1, ms2, ms3, max;

	timeval end;
	gettimeofday(&end, NULL);
	double microSec = end.tv_sec - p1.startTime.tv_sec;
	double sec = end.tv_usec - p1.startTime.tv_usec;
	ms1 = microSec * 1000.0 + sec / 1000.0;

	microSec = end.tv_sec - p2.startTime.tv_sec;
	sec = end.tv_usec - p2.startTime.tv_usec;
	ms2 = microSec * 1000.0 + sec / 1000.0;

	microSec = end.tv_sec - p3.startTime.tv_sec;
	sec = end.tv_usec - p3.startTime.tv_usec;
	ms3 = microSec * 1000.0 + sec / 1000.0;

	//max between p1, p2
	max = ms1;
	if (ms2 > max)
		max = ms2;

	//max between max and p3
	if (ms3 > max)
		max = ms3;

	sprintf(prodId, "%04d%04d%04d", p1.id, p2.id, p3.id);
	return max;
}

int main(int argc, char **argv)
{
	int Y = atoi(argv[1]);
	if (Y <= 0) {cout << "wrong Y value" << endl; return 0; }
	int pidCheck1 = atoi(argv[2]);
	if (pidCheck1 <= 0) {cout << "wrong pid value" << endl; return 0; }
	int pidCheck2 = atoi(argv[3]);
	if (pidCheck2 <= 0) {cout << "wrong pid value" << endl; return 0; }
	int pidCheck3 = atoi(argv[4]);
	if (pidCheck3 <= 0) {cout << "wrong pid value" << endl; return 0; }

	int semid = accessSem(9);
	int shmid = accessShmem(2*sizeof(Part));
	char* shmem = attachSharedMemory(shmid);

	cout << "Started Assembler " << endl;
	double allTimes = 0;

	for (int i = 0; i < Y; i++)
	{
		Part p1(0, true);
		Part p2(0, true);
		Part p3(0, true);

		down(semid, ready1);
		kill(pidCheck1, SIGUSR2);
		down(semid, written1);
		memcpy(&p1, shmem+sizeof(Part), sizeof(Part));
		cout << "Assembler: Part read "; p1.print(); cout << endl;

		down(semid, ready2);
		kill(pidCheck2, SIGUSR2);
		down(semid, written2);
		memcpy(&p2, shmem+sizeof(Part), sizeof(Part));
		cout << "Assembler: Part read "; p2.print(); cout << endl;

		down(semid, ready3);
		kill(pidCheck3, SIGUSR2);
		down(semid, written3);
		memcpy(&p3, shmem+sizeof(Part), sizeof(Part));
		cout << "Assembler: Part read "; p3.print(); cout << endl;

		usleep(S);
		char prodId[13];
		double assembledTime = getAssembleTime(p1, p2, p3, prodId);
		allTimes = allTimes + assembledTime;
		cout << "** Assembler: Assembled product " << i << " with id " << prodId << ": (total time: " << assembledTime << ") **" << endl;
	}
	kill(pidCheck1, SIGKILL);
	kill(pidCheck2, SIGKILL);
	kill(pidCheck3, SIGKILL);
	cout << "Ended Assembler with average total product time " << allTimes/Y << " ms." << endl;

	shmdt(shmem);
	return 0;
}
