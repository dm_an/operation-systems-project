#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <signal.h>
#include <queue>
#include "Semaphores.h"
#include "SharedMemory.h"
#include "Part.h"

using namespace std;

void sigusr1_handler(int sig);
void sigusr2_handler(int sig);

queue<Part> myqueue;
int Y;
int typeOfProducts;
int semid;
int shmid;
char* shmem;
static const unsigned d[] = {500, 600, 650};

int main(int argc, char **argv)
{
	Y = atoi(argv[1]);
	typeOfProducts = atoi(argv[2]); //typos proiontwn

	semid = accessSem(9);
	shmid = accessShmem(2*sizeof(Part));
	shmem = attachSharedMemory(shmid);

	//Install handler for SIGUSR1
	static struct sigaction act1;
	act1.sa_flags = 0;
	act1.sa_handler = sigusr1_handler;
	sigfillset(&act1.sa_mask);
	sigaction(SIGUSR1, &act1, NULL);

	//Install handler for SIGUSR2
	static struct sigaction act2;
	act2.sa_flags = 0;
	act2.sa_handler = sigusr2_handler;
	sigfillset(&act2.sa_mask);
	sigaction(SIGUSR2, &act2, NULL);

	/*
	signal(SIGUSR1, sigusr1_handler);
	signal(SIGUSR2, sigusr2_handler);
	*/
	cout << "Started Check " << typeOfProducts << endl;
	for (int i = 0; i < 2*Y; i++)
	{
		pause();
	}
	cout << "Started End " << typeOfProducts << endl;
	shmdt(shmem);
	return 0;
}

void sigusr1_handler(int sig)
{
//	cout << "Check received SIGUSR1" << endl;
	Part p(typeOfProducts, true);
	down(semid, paintedPart);
	memcpy(&p, shmem, sizeof(Part));
	memset(shmem, 0, sizeof(Part));
//	cout << "Check " << typeOfProducts << ": Part read "; p.print(); cout << endl;
	up(semid, paintAccess);
	usleep(d[p.type]);
	myqueue.push(p);
	if (typeOfProducts == 0)
		up(semid, ready1);
	else if (typeOfProducts == 1)
		up(semid, ready2);
	else if (typeOfProducts == 2)
		up(semid, ready3);
}

void sigusr2_handler(int sig)
{
//	cout << "Check received SIGUSR2" << endl;
	Part p = myqueue.front();
	myqueue.pop();
	memcpy(shmem+sizeof(Part), &p, sizeof(Part));
	if (typeOfProducts == 0)
		up(semid, written1);
	else if (typeOfProducts == 1)
		up(semid, written2);
	else if (typeOfProducts == 2)
		up(semid, written3);
}
