#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <signal.h>
#include <sys/wait.h>
#include "Semaphores.h"
#include "SharedMemory.h"
#include "Part.h"

using namespace std;

int main(int argc, char **argv)
{
	if (argc != 2)
	{
		cout << "Give 1 argument (Y)." << endl;
		return 1;
	}
	int Y = atoi(argv[1]);
	if (Y > 3000 || Y < 0)
	{
		cout << "Y value is wrong." << endl;
		return 1;
	}

	int semid = createSem(9, 0);
	char buf[10], bufp1[10], bufp2[10], bufp3[10];
	up(semid, paintAccess);

	int shmid = createShmem(2*sizeof(Part));
	char* shmem = attachSharedMemory(shmid);
	bzero(shmem, 2*sizeof(Part));

	cout << "Started Starter " << endl;
	int pid[8];
	for (int i = 0; i < 8; i++)
	{
		pid[i] = fork();
		if (pid[i] < 0) { perror("fork"); exit(0); }
		else if (pid[i] == 0)
		{
			switch(i)
			{
			case 0: //Construct 1-3
			case 1:
			case 2:
				sprintf(buf, "%d", i);
				execlp("./Construct", "Construct", argv[1], buf, NULL);
				perror("execlp");
				exit(0);
			case 3: //Check 1-3
			case 4:
			case 5:
				sprintf(buf, "%d", i-3);
				execlp("./Check", "Check", argv[1], buf, NULL);
				perror("execlp");
				exit(0);
			case 6:
				if (pid[3] != 0)
					sprintf(bufp1, "%d", pid[3]);
				if (pid[4] != 0)
					sprintf(bufp2, "%d", pid[4]);
				if (pid[5] != 0)
					sprintf(bufp3, "%d", pid[5]);
				usleep(11000);
				execlp("./Paint", "Paint", argv[1], bufp1, bufp2, bufp3, NULL);
				perror("execlp");
				exit(0);
			case 7:
				if (pid[3] != 0)
					sprintf(bufp1, "%d", pid[3]);
				if (pid[4] != 0)
					sprintf(bufp2, "%d", pid[4]);
				if (pid[5] != 0)
					sprintf(bufp3, "%d", pid[5]);
				usleep(11000);
				execlp("./Assembler", "Assembler", argv[1], bufp1, bufp2, bufp3, NULL);
				perror("execlp");
				exit(0);
			}
		}
	}

	int status;
	for (int i = 0; i < 8; i++)
		wait(&status);

	double allTimes=0, valueFromFile=0;
	char filename[10];
	for (int i = 0; i < 3; i++)
	{
		sprintf(filename, "%d.txt", pid[i]);
		FILE *in = fopen(filename, "r");
		fscanf(in, "%lf", &valueFromFile);
		allTimes += valueFromFile;
		fclose(in);
		unlink(filename);
	}

	shmdt(shmem);
	destroySem(semid);
	destroyShmem(shmid);
	cout << "Ended Starter. Average waiting time from Painter is " << allTimes/(3*Y) << " ms." << endl;
	return 0;
}
