#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include "Semaphores.h"
#include "SharedMemory.h"
#include "Part.h"

using namespace std;

int main(int argc, char **argv)
{
	int Y = atoi(argv[1]);
	int typeOfProducts = atoi(argv[2]); //typos proiontwn

	int semid = accessSem(9);
	int shmid = accessShmem(2*sizeof(Part));
	char* shmem = attachSharedMemory(shmid);

	double allTimes = 0.0;
	cout << "Started Construct " << typeOfProducts << endl;
	for (int i = 0; i < Y; i++)
	{
		Part p(typeOfProducts);
		cout << "Construct " << typeOfProducts << ": Part created "; p.print(); cout << endl;
		timeval start;
		gettimeofday(&start, NULL);
		down(semid, paintAccess);
		timeval end;
		gettimeofday(&end, NULL);
		double microSec = end.tv_sec - start.tv_sec;
		double sec = end.tv_usec - start.tv_usec;
		double msTime = microSec * 1000.0 + sec / 1000.0;
		allTimes += msTime;
		memcpy(shmem, &p, sizeof(Part));
		up(semid, partExists);
		usleep(rand() % 100);
	}

	cout << "Ended Construct " << typeOfProducts << " with average waiting time " << allTimes/Y << " ms." << endl;

	//Write allTimes to file for statistics
	char filename[10];
	sprintf(filename, "%d.txt", getpid());
	FILE* out = fopen(filename, "w");
	fprintf(out, "%lf\n", allTimes);
	fclose(out);

	shmdt(shmem);
	return 0;
}
