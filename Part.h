#ifndef PART_H_
#define PART_H_

#include <iostream>
#include <sys/stat.h>
#include <sys/time.h>

struct Part
{
	int id;
	int type;
	timeval startTime;
	Part(int typeOfProduct, bool dummy = false)
	{
		static int partId = 0;
		if (dummy == false)
			id = (typeOfProduct*1000) + partId++;
		gettimeofday(&startTime, NULL);
		type = typeOfProduct;
	}
	void print()
	{
		fprintf(stdout, "(%04d, %d)", id, type);
	}
};

#endif
