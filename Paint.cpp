#include <iostream>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <signal.h>
#include "Semaphores.h"
#include "SharedMemory.h"
#include "Part.h"

using namespace std;

static const unsigned k[] = {72300, 81200, 35000};

int main(int argc, char **argv)
{
	int Y = atoi(argv[1]);
	int pid[3];

	pid[0] = atoi(argv[2]);
	pid[1] = atoi(argv[3]);
	pid[2] = atoi(argv[4]);

	int semid = accessSem(9);
	int shmid = accessShmem(2*sizeof(Part));
	char* shmem = attachSharedMemory(shmid);

	cout << "Started Paint" << endl;
	for (int i = 0; i < 3 * Y; i++)
	{
		down(semid, partExists);
		Part p(0, true);
		memcpy(&p, shmem, sizeof(Part));
		if (p.type >= 0 && p.type <= 2)
			usleep(k[p.type]);
		up(semid, paintedPart);
		if (p.type >= 0 && p.type <= 2)
			kill(pid[p.type], SIGUSR1);
	}
	cout << "Ended Paint" << endl;
	shmdt(shmem);
	return 0;
}
